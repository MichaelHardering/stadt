﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StadtApplication.Models
{
    public class MemoryStadtContext : IStadtContext
    {
        public ICollection<Stadt> Staedte
        {
            get { return new List<Stadt>() {
                    new Stadt{ ID=1, Bundesland="NRW", Name="Essen"},  
                    new Stadt{ ID=2, Bundesland="NRW", Name="Duisburg"},
                    new Stadt{ ID=3, Bundesland="NRW", Name="Köln"}
            }; }
        }
    }
}