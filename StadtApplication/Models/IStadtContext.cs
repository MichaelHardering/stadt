﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StadtApplication.Models
{
    public interface  IStadtContext
    {

        ICollection<Stadt> Staedte { get; }

    }
}