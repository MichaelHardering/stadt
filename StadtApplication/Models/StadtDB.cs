﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StadtApplication.Models
{
    public class StadtDB : DbContext, IStadtContext
    {

        public StadtDB() : base("name=StadtDB") { }

        public DbSet<Stadt> Staedtes { get; set; }

        public ICollection<Stadt> Staedte
        {
            get { return Staedtes.ToList(); }
        }
    }
}