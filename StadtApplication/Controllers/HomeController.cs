﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StadtApplication.Models;

namespace StadtApplication.Controllers
{
    public class HomeController : Controller
    {

        private IStadtContext db = null;

        public HomeController(IStadtContext context)
        {
            db = context;
        }

        public ActionResult Index()
        {
            var test = db.Staedte.ToList();
            return View(test);
        }

        public ActionResult Csvhochaden() 
        {
            return View("Index");
        }
    }
}